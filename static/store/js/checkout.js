window.addEventListener("load", function() {
$(function (){
    $("#phone_id" ).keyup(function() {
        $(this).val($(this).val().replace(/ +?/g, ''));
        $(this).val($(this).val().replace(/[^0-9]/g, ''));
        var Phone = $("#phone_id").val()
    })

   
    var validator = $('#form_order').validate({
        onfocusout: function(element) {
            this.element(element);
            $(element).valid();
        },
        onkeyup: function(element) {
            this.element(element);
            $(element).valid();
        },
        onsubmit: function(element) {
          this.element(element);
          $(this).valid();
        },
        rules: {
            name :{
             required : true      
            },
            phone: {
                minlength: 10,
                maxlength: 10,
                digits: true,
                required :true,
            },
            address: {
              required :true
            },
            email :{
              required : true,
              email : true
              
            }

          },
        messages: {
            phone: {
                required: "Không được để trống",
                minlength: "Độ dài tối thiểu 10 ký tự",
                maxlength: "Số điện thoại không quá 10 ký tự",
                digits: "Vui lòng nhập số điện thoại có thực",
              
            },
           name: {
                required: "Tên khách hàng không được để trống",
            },
            email: {
              required: "Email không được để trống",
              email : "Vui lòng nhập một địa chỉ email hợp lệ"
            },
            address: {
              required: "Địa chỉ không được bỏ trống",
             
            }, 
          


        },
        highlight: function(element) {
            $(element).closest('.form-control').addClass('is-error');
        },
        unhighlight: function(element) {
                $(element).closest('.form-control').removeClass('is-error');
        },
  
    });
    });
});