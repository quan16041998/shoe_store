window.addEventListener("load", function() {
    let first_img_main = $(".product__thumb__pic:first").data('url')
    $('.nav-item').css("min -height",200);
    if(this.screen.width < 450 & this.screen.width>400){
        $('#padding-menu').css('padding-top','100px');
        let width = parseInt($('#tabs-1').width());
        $(".mobile-img").removeClass('product__thumb__pic')
        $(".mobile-img").addClass('product__thumb__pic__mobile')
        $(".mobile-img").css("min-width",width/4-1);
        $(".mobile-img").css("min-height",width/3);
        $('#first_img').addClass('order-first');
        $('#second_img').addClass('order-last');
    }
    if(this.screen.width < 400 ){
        let width = parseInt($('#tabs-1').width());
        $('#padding-menu').css('padding-top','100px');
        $(".mobile-img").removeClass('product__thumb__pic')
        $(".mobile-img").addClass('product__thumb__pic__mobile')
        $(".mobile-img").css("min-width",width/4-2);
        $(".mobile-img").css("min-height",width/3);
        $('#first_img').addClass('order-first');
        $('#second_img').addClass('order-last');
    }
    $('#img_main').attr('src',first_img_main);
    $('.mobile-img').click(function(e){
        let img_url = $(this).data('url')
        $('#img_main').attr('src',img_url);
    });
    $('.size_box ').click(function(e){
        $('.size_box_activate').addClass('size_box');
        $('.size_box_activate').removeClass('size_box_activate');
        $(this).removeClass('size_box')
        $(this).addClass('size_box_activate')
    });
    $('#btn-quantity-sub').click(function(e){
       let quantity = parseInt($('#id_quantity').val());
       if(quantity >1){
        $('#id_quantity').val(quantity-1);
       }
    });
    $('#btn-quantity-add').click(function(e){
        let quantity = parseInt($('#id_quantity').val());
        $('#id_quantity').val(quantity+1);
    });
    $('#hidden-size-alert').click(function(e){
        $('#alert_size').attr('hidden','');
    });  
    $('#add_to_cart').click(function (){
        let product_id = $(this).data('product');
        let count_cart = $('.item-in-cart').data('cart')
        let size =  $('.size_box_activate').data('size')
        let quantity = $('#id_quantity').val()
        let csrftoken = $('input[name="csrfmiddlewaretoken"]').val()
        if(size){
            $.ajaxSetup({
                beforeSend: function (xhr, settings) {
                    xhr.setRequestHeader("X-CSRFToken", csrftoken);
                }
            });
            $.ajax({
                type: 'POST',
                url: '../../add_cart/',
                data : {
                    'product_id' : product_id,
                    'size' : size,
                    'quantity' : quantity
                },
                success: function (data) {  
                    if(data.status == 404){
    
                        $('#alert_product').removeClass('alert-success');
                        $('#alert_product').addClass('alert-danger');
                        $('#alert_mess').html(data.message);
                        $('#alert_product').removeAttr('hidden');
                    }else{
                        $('#alert_mess').html(data.message);
                        $('#alert_product').removeAttr('hidden');
                        $('.item-in-cart').html(count_cart+1)
                    };
                },
                error: function (data) {
                    console.log(data);
                }
            });
        }else{
            $('#alert_size').removeAttr('hidden');
        }
    });



});