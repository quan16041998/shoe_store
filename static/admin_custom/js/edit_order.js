window.addEventListener("load", function() {
    $('.img-circle').on("click", function(e){
        let status = $(this).data('status');
        $('.img-circle').removeClass('active');
        $(this).addClass('active');
        $('.order-status-timeline-completion').css("width", status)
        $('#id_status').val($(this).data('check'));

    });
    let value_status = $('#id_status').find(":selected").val();
    $("*[data-check=" + value_status+"]").click();
    $('textarea').val('').trigger('input');


      
});