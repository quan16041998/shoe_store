window.addEventListener("load", function() {
    let total_variant = $("[name='product_variants_set-TOTAL_FORMS']").val();
    console.log(total_variant,'====================================')
    $("#add_variant").on("click", function(e){
      let total_variant = $("[name='product_variants_set-TOTAL_FORMS']").val();
      let check = $('#form_add_variant_product')
      console.log(check)
      $('#form_add_variant_product td select').attr('name', 'product_variants_set-'+ total_variant +'-size');
      $('#form_add_variant_product td select').attr('id', 'id_product_variants_set-'+ total_variant +'-size');
      $('#form_add_variant_product td input[type="number"]').attr('name', 'product_variants_set-'+ total_variant +'-quantity');
      $('#form_add_variant_product td input[type="number"] ').attr('id', 'id_product_variants_set-'+ total_variant +'-quantity');
      $('#form_add_variant_product td input[type="text"]').attr('name', 'product_variants_set-'+ total_variant +'-sku');
      $('#form_add_variant_product td input[type="text"]').attr('id', 'id_product_variants_set-'+ total_variant +'-sku');
       $("[name='product_variants_set-TOTAL_FORMS']").val(parseInt(total_variant)+1);
       let add_form = $('#form_add_variant_product').clone();
       add_form.removeAttr('hidden');
       add_form.removeAttr('id');
       add_form.addClass('form_set_variant')
       $('.form_set_variant:last').after(add_form)
    });
    $("#add_img").on("click", function(e){
      let total_variant_img = $("[name='img_product_set-TOTAL_FORMS']").val();
      $('#form_img td input[type="file"]').attr('name', 'img_product__set-'+ total_variant_img +'-img');
      $('#form_img td input[type="file"]').attr('id', 'id_img_product__set-'+ total_variant_img +'-img');
       $("[name='img_product_set-TOTAL_FORMS']").val(parseInt(total_variant_img)+1);
       let add_form = $('#form_img').clone();
       add_form.removeAttr('hidden');
       add_form.removeAttr('id');
       add_form.addClass('form_set_varian')
       $('.form_set_img:last').after(add_form)
    });
    $("body").on("click",".btn-remove-variant", function(e){
        $(this).closest('tr').empty();
        let variant =  $(this).data('variant');
        let delete_input_id =  '#id_product_variants_set-'+ variant+'-DELETE'
        let total_variant = $("[name='product_variants_set-TOTAL_FORMS']").val();
        if ($(delete_input_id).is(":checked")){
            $(delete_input_id).prop('checked', false);
        }else{
            $(delete_input_id).prop('checked', true);
        };
        $("[name='product_variants_set-TOTAL_FORMS']").val(parseInt(total_variant)-1);
    });
    $("body").on("click",".btn-remove-img", function(e){
        $(this).closest('tr').empty();
        console.log($(this).parent())
        let img =  $(this).data('img');
        let delete_img_id =  '#id_img_product_set-'+ img+'-DELETE';
        let total_img = $("[name='img_product_set-TOTAL_FORMS']").val();

        if ($(delete_img_id).is(":checked")){
            $(delete_img_id).prop('checked', false);
        }else{
            $(delete_img_id).prop('checked', true);
        }
        $("[name='img_product_set-TOTAL_FORMS']").val(parseInt(total_img)-1);
    });

    // #validated//
    var validator = $('#form_add_product').validate({
        onfocusout: function(element) {
            this.element(element);
            $(element).valid();
        },
        onkeyup: function(element) {
            this.element(element);
            $(element).valid();
        },
        onsubmit: function(element) {
          this.element(element);
          $(this).valid();
        },
        rules: {
            name :{
              required : true      
            },
            category: {
                required :true,
            },
            price: {
                required :true,
                digits : true
            },
            accurate_price: {
                required :true,
                digits : true
            },
            fabric: {
                required :true,
            },
            fabric: {
                required :true,
            },
            describe: {
                required :true,
                maxlength: 255
            }, 

          },
        messages: {
            category: {
                required: "Danh mục không được để trống",      
            },
            name: {
                required: "Tên sản phẩm không được để trống",
            },
            price: {
                required: "Giá gốc không được để trống",
                digits : "Giá phải là số dương"
            },
            accurate_price: {
                required: "Giá hiện tại không được để trống",
                digits : "Giá phải là số dương"
            },
            fabric: {
                required: "Chất liệu không được để trống",
            },
            describe: {
                required: "Miêu tả không được để trống",
                maxlength : "Độ dài vượt quá giới hạn"
            },
          


        },
        highlight: function(element) {
            $(element).closest('.form-control').addClass('is-error');
        },
        unhighlight: function(element) {
                $(element).closest('.form-control').removeClass('is-error');
        },
  
    });
    //js lặt vặt tăng trải nghiệm
    $('#id_sale_percent').change(function (){
        let price = $('#id_price').val();
        let sale_percent = $(this).find('option:selected').val();
        if (sale_percent == '0'){
            $('#id_accurate_price').val(price)
            $('#id_status').val('normal');
        }else{
            $('#id_status').val('sale');
        }
        $('#id_accurate_price').val(Math.floor(parseInt(price)*(100-parseInt(sale_percent))/100))
    });
    $('#id_price').change(function (){
        let price = $(this).val();
        let sale_percent = $('#id_sale_percent').find('option:selected').val();
        $('#id_accurate_price').val(Math.floor(parseInt(price)*(100-parseInt(sale_percent))/100))
    });
    function readURL(input) {
        if (input.files && input.files[0]) {
          var reader = new FileReader();
          reader.onload = function(e) {
            $('#previewHolder').attr('src', e.target.result);
          }
          reader.readAsDataURL(input.files[0]);
        } else {
          alert('select a file to see preview');
          $('#previewHolder').attr('src', '');
        }
      }
    $('input[type=file]').on("change", function(e){
       readURL(this)
    })


  });
