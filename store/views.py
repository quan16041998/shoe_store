from django.shortcuts import render,redirect
from admin_custom.models import *
from django.urls import reverse
from django.core.paginator import Paginator
from django.http import JsonResponse
from http import HTTPStatus
from django.core.exceptions import ValidationError
from django.core.validators import validate_email
from django.contrib import messages
# Create your views here.

def main_shop(request):
    count_cart = 0
    banner = Banner.objects.filter(is_activate = True).order_by('order')
    list_product_new =  Product.objects.filter(activate = True).order_by("-created_at")[:6]
    context = {
        'banner' : banner,
        'count_cart' : count_cart,
        'list_product_new' : list_product_new,
    }
    return render(request,"store/index.html",context)

def shop(request,category=None):
    sort_type= request.GET.get('sort_type', 'all')
    if category is not None and category != 'all':
        if sort_type == "low":
            list_product = Product.objects.filter(activate = True,category__title= category).order_by("accurate_price")
        elif sort_type == "high":
            list_product = Product.objects.filter(activate = True, category__title=category).order_by("-accurate_price")
        else:
            list_product = Product.objects.filter(activate = True,category__title=category).order_by('name')
    elif category == 'all' or category is None:
        if sort_type == "low":
            list_product = Product.objects.filter(activate = True).order_by("accurate_price")
        elif sort_type == "high":
            list_product = Product.objects.filter(activate = True).order_by("-accurate_price")
        else:
            list_product = Product.objects.filter(activate = True).order_by('name')
    product_paginator = Paginator(list_product,12)
    page_num = request.GET.get('page')
    
    page = product_paginator.get_page(page_num)
    if category == 'all' or category is None:
        context = {
            'count' : product_paginator.count,
            'page' : page,
            'sort_type' : sort_type,
            'category_name' : 'Tất Cả',
            'category' : category,
        }
    else:
         context = {
            'count' : product_paginator.count,
            'page' : page,
            'sort_type' : sort_type,
            'category_name' : category,
            'category' : category,
        }
    return render(request,"store/shop.html",context)

def product_detail(request,product_id):
        about = About_contact.objects.first()
        product = Product.objects.filter(id=product_id).first()
        product_size = product_variants.objects.filter(product_id = product_id)
      
        if product is not None:
            data_size = []
            for item in product_size:
                data_size.append(item.size.size)
            data_size.sort()
            img = Img_product.objects.filter(id_product = product_id)[:3]
            if product.activate == False:
                return redirect(reverse('shoe_store:shop'))
            context = { 
                'product' : product,
                'img' : img,
                'about' : about,
                'product_size'   : data_size,
            }
            return render(request,"store/product-details.html",context)
        else:
            return redirect(reverse('shoe_store:shop', kwargs={'category' : None}))

def cart(request):
    cart_id = None
    if 'cart' in request.session and request.session['cart'] and request.session['cart'] is not None:
        list_product = request.session['cart']
        total_price = 0
        for item in list_product:
            total_price += int(item['price'])
        context = {
            'list_product' : list_product,
            'total_price'  : total_price,
        }    
        return render(request,"store/cart.html",context)
    else:
         return render(request,"store/no_cart.html")

def checkout(request):
    if 'cart' in request.session and request.session['cart'] and request.session['cart'] is not None:
        list_product = request.session['cart']
        total_price = 0
        for item in list_product:
            total_price += int(item['price'])
        if request.method == 'POST':
            name =request.POST.get('name')
            address =request.POST.get('address')
            email =request.POST.get('email')
            phone_number =request.POST.get('phone')
            order_notes =request.POST.get('order_notes')
            check_email = None
            try:
                validate_email(email)
            except ValidationError as e:
                print("bad email, details:", e)
            else:
                check_email = True
            if name is not None and address is not None and check_email == True and phone_number is not None:
                try:
                    order =  Order.objects.create(customer_name = name, shipping_address = address,                           
                                                phone_number = phone_number, email = email, order_description = order_notes,total_price = total_price)
                    if order:
                        for item in list_product:
                            product_order = Product.objects.get(id = item['id'] )
                            if product_order:
                                order_detail =Order_detail.objects.create(order = order, product = product_order, size= item['size'],quantity = item['quantity'])
                            else:
                                Order.objects.delete(id = order.id)
                            del request.session['cart']
                            messages.success(request, ('Bạn đã đặt hàng thành công!'))
                            return redirect(reverse('shoe_store:shop',kwargs={'category' : None}))
                    else:
                        messages.error(request, ('Bạn đã đặt hàng thất bại!'))
                        return redirect(reverse('shoe_store:checkout'))  
                except Exception as e:
                    print(e)
                    messages.error(request, ('Bạn đã đặt hàng thất bại!'))
                    return redirect(reverse('shoe_store:checkout'))  
        context = {
            'list_product' : list_product,
            'total_price'  : total_price,
        }     
        return render(request,"store/checkout.html",context)
    else:
        return redirect(reverse('shoe_store:shop', kwargs={'category' : None}))
       
def add_cart(request):
    if request.is_ajax:
        product_id = request.POST.get('product_id')
        size = request.POST.get('size')
        quantity = request.POST.get('quantity')
        if 'cart' in request.session and request.session['cart'] and request.session['cart'] is not None:
            list_product = request.session['cart']
        else:
            list_product =[]
        if product_id:
            if list_product is not None:
                for item in list_product:
                    if item['id'] == product_id:
                         return JsonResponse({
                        'status': HTTPStatus.NOT_FOUND,
                        'message':'Sản phẩm đã có trong giỏ hàng !',
                        })
            product = Product.objects.get(id = product_id)
            img = Img_product.objects.filter(id_product = product_id).first()
            true_price = int(product.accurate_price) * int(quantity)
            id_product ={
               'id' : product_id,
               'name' : product.name,
               'url_img' : img.img.url,
               'price'  : true_price,
                'size' : size,
                'quantity' : quantity
            }
            list_product.append(id_product)
            request.session['cart'] = list_product
            print( HTTPStatus.OK)
            return JsonResponse({
                    'status': HTTPStatus.OK,
                    'message':'Sản phẩm được thêm vào giỏ hàng !',
                })
            
def remove_cart(request):
    if request.is_ajax :
        product_id = request.GET.get('product_id')
        if 'cart' in request.session and request.session['cart'] and request.session['cart'] is not None:
            list_cart = request.session['cart']
            new_list_cart = [item for item in list_cart if item['id'] != product_id]
            request.session['cart']  = new_list_cart
            option = ''
            total_price = 0
            for value in new_list_cart:
                total_price += int(value['price'])
                option += '<tr><td class="product-thumbnail"><img src="' + str(value['url_img']) +'"alt="Image" class="img-fluid"></td><td class="product-name"><h5>' + str(value['name']) + '</h5></td><td>'  + str(value['size']) + '</td><td>'+str(value['quantity'])+ '</td><td>' + str(value['price']) + '</td><td><a class="btn btn-black btn-sm btn-delete-cart-ajax" data-id="' + str(value['id']) +'">X</a></td></tr>'
            context = {
                'html': option,
                'total_price' : total_price
            }
            return JsonResponse(context)
        context = {
            'message': 'Invalid request'
        }
        return JsonResponse(context)
    
def search(request):
    check = False
    if request.method == "POST":
        name = request.POST.get('search')
        list_product = Product.objects.filter(name__contains = name)
        product_paginator = Paginator(list_product,12)
        page_num = request.GET.get('page')
        page = product_paginator.get_page(page_num)
        if list_product is not None:
            check = True
        context = {
            'count' : product_paginator.count,
            'page' : page,
            'check' : check
        }
        return render(request,"store/search.html",context)
    return redirect(reverse('shoe_store:index'))

# Create your views here.
