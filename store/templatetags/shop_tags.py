from django import template
register = template.Library()
from admin_custom.models import Img_product, Category,About_contact,Product


@register.simple_tag(name='get_img_product')
def get_img_product(id_product):
    img = Img_product.objects.filter(id_product = id_product).first()
    if img:
        return img.img.url
    return None

@register.inclusion_tag('store/navbar.html')
def render_category(request):
    category = Category.objects.all()
    return { 'request' :request, 'category': category }

@register.inclusion_tag('store/sale_product.html')
def render_sale_product():
    list_product_sale = Product.objects.filter(activate = True, status = 'sale').order_by("accurate_price")[:10]
    return {'list_product_sale' : list_product_sale}

@register.inclusion_tag('store/footer.html')
def render_footer():
    about = About_contact.objects.first()
    return {'about' : about}
