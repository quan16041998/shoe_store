from django.urls import path
from . import views
app_name='shoe_store'
urlpatterns = [
    path('',views.main_shop,name='index'),
    path('trang-chu/',views.main_shop,name='index'),
    path('cua-hang/<slug:category>/', views.shop,name ="shop"),
    path('san-pham/<int:product_id>/', views.product_detail,name= "product_detail"),
     path('tim-kiem/', views.search,name= "search"),
    path('gio-hang/', views.cart,name= "cart"),
    path('thanh-toan/',views.checkout,name="checkout"),
    path('add_cart/',views.add_cart,name="add_cart"),
    path('remove_cart/',views.remove_cart,name="remove_cart"),
]