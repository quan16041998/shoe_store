from django import forms
from .models import *
from django.forms import BaseInlineFormSet,inlineformset_factory
from django.utils.translation import gettext_lazy as _

class FormProduct(forms.ModelForm):
    class Meta:
        model = Product
        fields = '__all__'
    name =  forms.CharField( widget=forms.TextInput(attrs={'class': 'form-control col-md-3 mb-2'}),label="Tên sản phẩm")   
    category = forms.ModelChoiceField(queryset=Category.objects.all(),widget=forms.Select(attrs={'class': 'form-control col-md-3 mb-2'}),label="Danh mục")
    price =  forms.CharField( widget=forms.NumberInput(attrs={'class': 'form-control col-md-3 mb-2'}),label="Giá gốc")
    accurate_price =  forms.CharField( widget=forms.NumberInput(attrs={'class': 'form-control col-md-3 mb-2'}),label="Giá chính thức")
    fabric =  forms.CharField( widget=forms.TextInput(attrs={'class': 'form-control col-md-3 mb-2'}),label="Chất liệu")     
    describe =  forms.CharField( widget=forms.TextInput(attrs={'class': 'form-control col-md-3 mb-2'}),label="Miêu tả sản phẩm")
    sale_percent = forms.ChoiceField(choices=SALE_PERCENT,widget=forms.Select(attrs={'class': 'form-control col-md-3 mb-2'}),label="%SALE")
    status = forms.ChoiceField(choices=STATUS,widget=forms.Select(attrs={'class': 'form-control col-md-3 mb-2'}),label="Trạng thái")
class FormInlineProduct(forms.ModelForm):
    class Meta:
        model = product_variants
        fields = '__all__'
 
    size = forms.ModelChoiceField(queryset=size_product.objects.all(),widget=forms.Select(attrs={'class': 'form-control col-md-4 mb-2'}),label="Kích cỡ")
    quantity =  forms.IntegerField( widget=forms.NumberInput(attrs={'class': 'form-control col-md-4 mb-2'}),label="Số lượng")   
    sku =  forms.CharField( widget=forms.TextInput(attrs={'class': 'form-control col-md-4 mb-2'}),label="Mã sản phẩm")
class FormInlineImg(forms.ModelForm):
    class Meta:
        model = Img_product
        fields = '__all__'
 
    img= forms.ImageField(allow_empty_file=False,label="Ảnh sản phẩm")
  
class FormOrder(forms.ModelForm):
    class Meta:
        model = Order
        fields = '__all__'
    customer_name =  forms.CharField( widget=forms.TextInput(attrs={'class': 'form-control col-md-3 mb-2'}),label="Tên khách hàng")   
    shipping_address =  forms.CharField( widget=forms.TextInput(attrs={'class': 'form-control col-md-3 mb-2'}),label="Địa chỉ nhận đơn")   
    phone_number =  forms.CharField( widget=forms.NumberInput(attrs={'class': 'form-control col-md-3 mb-2'}),label="Số điện thoại khách hàng")   
    email =  forms.EmailField( widget=forms.EmailInput(attrs={'class': 'form-control col-md-3 mb-2'}),label="Email khách hàng")
    total_price = forms.CharField( widget=forms.NumberInput(attrs={'class': 'form-control col-md-3 mb-2','readonly':''}),label="Tổng tiền đơn hàng")   
    order_description    =  forms.CharField( widget=forms.Textarea(attrs={'class': 'form-control col-md-3 mb-2'}),label="Ghi chú", required=False)
    status  = forms.ChoiceField(choices=STATUS_ORDER,widget=forms.Select(attrs={'class': 'form-control col-md-3 mb-2','hidden' :''}),label='')
    
class FormInlineOrder(forms.ModelForm):
    class Meta:
        model = Order_detail
        fields = '__all__'
    product = forms.ModelChoiceField(queryset=Product.objects.all(),widget=forms.Select(attrs={'class': 'form-control col-md-4 mb-2','disabled':''}),label="Tên giày")
    size = forms.IntegerField( widget=forms.NumberInput(attrs={'class': 'form-control  mb-2'}),label="Size")   
    quantity =  forms.IntegerField( widget=forms.NumberInput(attrs={'class': 'form-control mb-2'}),label="Số lượng")   
    def image_tag(self):
            img = Img_product.objects.filter(id_product = self.product).first()
            return mark_safe('<img src="%s" width="125" height="150" />' % (img.img.url))
    image_tag.short_description = 'Image'

class FormBanner(forms.ModelForm):
    class Meta:
        model = Banner
        fields = '__all__'
    title =  forms.CharField( widget=forms.TextInput(attrs={'class': 'form-control col-md-3 mb-2'}),label="Tiêu đề")  
    order = forms.IntegerField( widget=forms.NumberInput(attrs={'class': 'form-control  mb-2'}),label="Thứ tự")   
    
class FormSize(forms.ModelForm):
    class Meta:
        model = size_product
        fields = '__all__'
    size = forms.IntegerField( widget=forms.NumberInput(attrs={'class': 'form-control  mb-2'}),label="Size",required=True)   
    
class FormCategory(forms.ModelForm):
    class Meta:
        model = Category
        fields = '__all__'
    title = forms.CharField( widget=forms.TextInput(attrs={'class': 'form-control col-md-3 mb-2 title-category'}),label="Tiêu đề",required=True)  
    slug = forms.CharField( widget=forms.TextInput(attrs={'class': 'form-control col-md-3 mb-2'}),label="Miêu tả",required=True)  

class FormAboutContact(forms.ModelForm):
    class Meta:
        model = About_contact
        fields = '__all__'
    title =  forms.CharField( widget=forms.TextInput(attrs={'class': 'form-control col-md-3 mb-2'}),label="Tiêu đề") 
    about_describe =  forms.CharField( widget=forms.Textarea(attrs={'class': 'form-control col-md-3 mb-2'}),label="Lời tựa")  
    last_mes =  forms.CharField( widget=forms.TextInput(attrs={'class': 'form-control col-md-3 mb-2'}),label="Lời cuối")  
    facebook =  forms.CharField( widget=forms.TextInput(attrs={'class': 'form-control col-md-3 mb-2'}),label="Facebook")  
    instagram =  forms.CharField( widget=forms.TextInput(attrs={'class': 'form-control col-md-3 mb-2'}),label="Instagram")  
 



    
ProductFormsetVariant = inlineformset_factory(Product,product_variants,fields=('size','quantity','sku',),extra=1,form= FormInlineProduct,can_delete=True)
ProductFormsetImg = inlineformset_factory(Product,Img_product,fields=('img',),extra=3, form=FormInlineImg)
OrderFormset = inlineformset_factory(Order, Order_detail,fields = ('product','size','quantity',),extra=0,form=FormInlineOrder,exclude=('img_tag',))
