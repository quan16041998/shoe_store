from django.shortcuts import render,redirect
from django.urls import reverse
from django.contrib.auth.models import User
from .models import *
from django.contrib.auth import authenticate, login,logout
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib.auth.decorators import login_required
from .forms import *
from django.forms import inlineformset_factory
from django.contrib import messages
from django.conf import settings
from django.contrib.auth.models import User
from django.core.paginator import Paginator
# Create your views here.
nav_activate  = 'bg-gradient-primary'
@login_required(login_url=settings.LOGIN_ULR, redirect_field_name=settings.REDIRECT_FIELD_NAME)
def product_table(request):
    sort_type = request.GET.get('sort_type', 'all')
    print(sort_type)
    if sort_type == "all":
        list_product = Product.objects.filter(activate = True).order_by('created_at')
    elif sort_type == "sale":
        list_product = Product.objects.filter(activate = True, status ='sale').order_by('created_at')
    else:
        list_product = Product.objects.filter(activate = True, category__id = sort_type).order_by('created_at')
        sort_type = int(sort_type) 
    category = Category.objects.filter(activate = True)
    context = {
        'list_product' : list_product,
        'category' : category,
        'sort_type' : sort_type,
        'product_activate' :  nav_activate
    }
    return render(request,'admin_custom/admin_product.html',context)

@login_required(login_url=settings.LOGIN_ULR, redirect_field_name=settings.REDIRECT_FIELD_NAME)
def add_product(request):
    size = size_product.objects.all()
    if request.method == "POST":
        form = FormProduct(request.POST)
        form_set = ProductFormsetVariant(request.POST or None)
        form_set_img = ProductFormsetImg(request.POST,request.FILES)
      
        if form.is_valid() and form_set.is_valid() and form_set_img.is_valid():
            new_product_id = form.save()
            form_set.save(commit=False)
            form_set_img.save(commit=False)
            form_set_img.instance = new_product_id
            form_set_img.save()
            form_set.instance = new_product_id
            form_set.save()
            messages.success(request, ('Thêm mới thành công!'))
            return redirect(reverse('admin_custom:admin_product'))
        else:
            messages.error(request, ('Đã xảy ra lỗi!'))
            context = {
            'form' : form,
            'size' : size,
            'form_set' : form_set,
            'form_set_img' : form_set_img,
            'product_activate' :  nav_activate
                } 
            return render(request,'admin_custom/add_product.html',context)
    form = FormProduct()
    form_set = ProductFormsetVariant
    form_set_img = ProductFormsetImg
    context = {
        'form' : form,
        'size' : size,
        'form_set' : form_set,
        'form_set_img' : form_set_img
    }   
    return render(request,'admin_custom/add_product.html',context)

@login_required(login_url=settings.LOGIN_ULR, redirect_field_name=settings.REDIRECT_FIELD_NAME)
def edit_product(request,product_id):
    product = Product.objects.get(id = product_id)
    size = size_product.objects.all().order_by('size')
    if request.method == "POST":
        form = FormProduct(request.POST,instance=product)
        form_set = ProductFormsetVariant(request.POST or None,instance=product)
        form_set_img = ProductFormsetImg(request.POST,request.FILES,instance=product)
        if form.is_valid() and form_set.is_valid() and form_set_img.is_valid():
            form.save()
            form_set_img.save()
            form_set.save()
            messages.success(request, ('Chỉnh sửa thành công!'))
            return redirect(reverse('admin_custom:admin_product'))
        else:
            context = {
            'product_id' : product_id,
            'form' : form,
            'size' : size,
            'form_set' : form_set,
            'form_set_img' : form_set_img
                } 
            return render(request,'admin_custom/add_product.html',context)

    form = FormProduct(instance=product)
    form_set = ProductFormsetVariant(instance=product)
    form_set_img = ProductFormsetImg( instance=product)
    context = {
        'product_id' : product_id,
        'form' : form,
        'size' : size,
        'form_set' : form_set,
        'form_set_img' : form_set_img,
    } 
    return render(request,'admin_custom/edit_product.html',context)
@login_required(login_url=settings.LOGIN_ULR, redirect_field_name=settings.REDIRECT_FIELD_NAME)
def delete_product(request):
    if request.method == "POST":
        product_id= request.POST.get('product_id_delete')
        print(product_id)
        if product_id is not None:
            try:
                Product.objects.get(id=product_id).delete()
            except Exception as e:
                print(e)
            messages.success(request, ('Xóa sản phẩm thành công!'))
        else:
            messages.error(request, ('Sản phẩm chưa thể xóa!'))
        return redirect(reverse('admin_custom:admin_product'))
    else:
        return redirect(reverse('admin_custom:admin_product')) 
    
    
@login_required(login_url=settings.LOGIN_ULR, redirect_field_name=settings.REDIRECT_FIELD_NAME)
def order_table(request):
    status = STATUS_ORDER
    sort_type = request.GET.get('sort_type', 'all')
    if sort_type == "all":
        order = Order.objects.all().order_by('-order_at')
    else:
        order = Order.objects.filter(status  = sort_type).order_by('-order_at')
    context ={
        'order' : order,
        'sort_type' : sort_type,
        'status' : status,
        'order_activate' :  nav_activate
    }
    return render(request,'admin_custom/admin_order.html',context)

@login_required(login_url=settings.LOGIN_ULR, redirect_field_name=settings.REDIRECT_FIELD_NAME)
def edit_order(request,order_id):
    size = size_product.objects.all()
    order = Order.objects.get(id= order_id)
    form = FormOrder(request.POST or None,instance = order)
    order_detail  =Order_detail.objects.filter(order = order_id)
    form_set = OrderFormset(request.POST or None,instance = order)
    if request.method == "POST":
        if form.is_valid():
            form.save()
            messages.success(request, ('Chỉnh sửa thành công!'))
            return redirect(reverse('admin_custom:admin_order'))
        else:
            context = {
            'form' : form,
            'size' : size,
            'order_id' : order_id,
            'order_detail' : order_detail 
            }
            return render(request,'admin_custom/edit_order.html',context)
    context = {
        'form' : form,
        'form_set' : form_set,
        'size' : size,
        'order_id' : order_id,
        'order_detail' : order_detail 
     }
    return render(request,'admin_custom/edit_order.html',context)
    
@login_required(login_url=settings.LOGIN_ULR, redirect_field_name=settings.REDIRECT_FIELD_NAME)
def delete_order(request):
    if request.method == "POST":
        order_id =  request.POST.get('order_id_delete')
        if order_id is not None:
            try:
                Order.objects.get(id=order_id).delete()
            except Exception as e:
                print(e)
            messages.success(request, ('Xóa đơn hàng thành công!'))
        else:
            messages.error(request, ('Đơn hàng chưa thể xóa!'))
        return redirect(reverse('admin_custom:admin_order'))
    else:
        return redirect(reverse('admin_custom:admin_order')) 
    
@login_required(login_url=settings.LOGIN_ULR, redirect_field_name=settings.REDIRECT_FIELD_NAME)
def admin_other(request):
    about = About_contact.objects.all()
    about_instance =  About_contact.objects.all().first()
    size = size_product.objects.all().order_by('size')
    category = Category.objects.all().order_by('id')
    banner =  Banner.objects.all().order_by('order')
    form_about = FormAboutContact(instance=about_instance)
    form_size = FormSize()
    form_category = FormCategory()
    form_banner = FormBanner()
    context = {
    'about' : about,
    'size' :size,
    'banner' : banner,
    'other_activate' :  'other_activate',
    'form_about' :  form_about,
    'form_size'  : form_size,
    'form_banner' : form_banner,
    'category' : category,
    'form_category' : form_category 
    }
    return render(request,'admin_custom/other_table.html',context)

@login_required(login_url=settings.LOGIN_ULR, redirect_field_name=settings.REDIRECT_FIELD_NAME)
def delete_other(request):
    if request.method == "POST":
        other_id =  request.POST.get('other_id_delete')
        check_delete = request.POST.get('check_delete', None)
        if other_id is not None and check_delete is not None:
            if check_delete == 'about':
                try:
                    About_contact.objects.get(id=other_id).delete()
                except Exception as e:
                    print(e)
                messages.success(request, ('Xóa thành công!'))
            elif check_delete == 'size':
                try:
                    size_product.objects.get(id=other_id).delete()
                except Exception as e:
                    print(e)
                messages.success(request, ('Xóa thành công!'))       
            elif check_delete == 'banner':
                try:
                    Banner.objects.get(id=other_id).delete()
                except Exception as e:
                    print(e)
                messages.success(request, ('Xóa thành công!'))
            else:
                try:
                    Category.objects.get(id=other_id).delete()
                except Exception as e:
                    print(e)
                messages.success(request, ('Xóa thành công!'))
        else:
            messages.error(request, ('Chưa thể xóa!'))
        return redirect(reverse('admin_custom:admin_other'))
    else:
        return redirect(reverse('admin_custom:admin_other')) 

@login_required(login_url=settings.LOGIN_ULR, redirect_field_name=settings.REDIRECT_FIELD_NAME)
def edit_about_contact(request):
    if request.method == "POST":
        id_about = request.POST.get('edit_about')
        about = About_contact.objects.get(id = id_about)
        form = FormAboutContact(request.POST or None, instance=about)
        if form.is_valid:
            form.save()
            messages.success(request, ('Sửa thành công!'))
            return redirect(reverse('admin_custom:admin_other')) 
        else:
            messages.error(request, ('Sửa thất bại'))
            return redirect(reverse('admin_custom:admin_other'))
         
@login_required(login_url=settings.LOGIN_ULR, redirect_field_name=settings.REDIRECT_FIELD_NAME)
def add_size(request):    
    if request.method == "POST":
        id_size = request.POST.get('check_size')
        if id_size is not None:
            try:
                size  = size_product.objects.get(id = id_size)
                form = FormSize(request.POST or None, instance=size)
            except:
                form = FormSize(request.POST or None)
        else:
            form = FormSize(request.POST or None)
        if form.is_valid:
            form.save()
            messages.success(request, ('Thêm hoặc sửa thành công!'))
            return redirect(reverse('admin_custom:admin_other'))  
        else:
            messages.error(request, ('Thêm hoặc sửa thất bại'))
            return redirect(reverse('admin_custom:admin_other')) 
        
@login_required(login_url=settings.LOGIN_ULR, redirect_field_name=settings.REDIRECT_FIELD_NAME)            
def add_banner(request):
    if request.method == "POST":
        form = FormBanner(request.POST or None, request.FILES)   
        if form.is_valid:
            form.save()
            messages.success(request, ('Thêm hoặc sửa thành công!'))
            return redirect(reverse('admin_custom:admin_other'))  
        else:
            print(form.errors)
            messages.error(request, ('Thêm hoặc sửa thất bại'))
            return redirect(reverse('admin_custom:admin_other')) 
        
@login_required(login_url=settings.LOGIN_ULR, redirect_field_name=settings.REDIRECT_FIELD_NAME)            
def edit_banner(request):
    if request.method == "POST":
        id = request.POST.get('edit_banner')
        if id is not None:
            banner = Banner.objects.get(id = id)
            form = FormBanner(request.POST or None, request.FILES, instance=banner)   
            if form.is_valid:
                form.save()
                messages.success(request, ('Sửa ảnh bìa thành công!'))
                return redirect(reverse('admin_custom:admin_other'))  
            else:
                messages.error(request, ('Sửa ảnh bìa thất bại'))
                return redirect(reverse('admin_custom:admin_other')) 
        else:
            messages.error(request, ('Sửa ảnh bìa thất bại'))
            return redirect(reverse('admin_custom:admin_other')) 
        
@login_required(login_url=settings.LOGIN_ULR, redirect_field_name=settings.REDIRECT_FIELD_NAME)
def add_category(request):    
    if request.method == "POST":
        id_size = request.POST.get('check_category')
        if id_size is not None:
            try:
                size  = Category.objects.get(id = id_size)
                form = FormCategory(request.POST or None, instance=size)
            except:
                form = FormCategory(request.POST or None)
        else:
            form = FormCategory(request.POST or None)
        if form.is_valid:
            form.save()
            messages.success(request, ('Thêm hoặc sửa thành công!'))
            return redirect(reverse('admin_custom:admin_other'))  
        else:
            messages.error(request, ('Thêm hoặc sửa thất bại'))
            return redirect(reverse('admin_custom:admin_other')) 


                
def sign_in(request):
    msg = None
    if request.method == 'POST':
        username =request.POST.get('username')
        password =request.POST.get('passwork')
        user = authenticate(username=username, password=password)
        if user is not None and user.is_superuser :
            login(request,user)
            response = HttpResponseRedirect(
                        reverse('admin_custom:admin_product'))
            return response
        else:
            msg = 'Sai tài khoản hoặc mật khẩu'
    return render(request,'admin_custom/sign-in.html',{"msg": msg})

def sign_out(request):
    logout(request)
    response = HttpResponseRedirect('/login')
    return response

@login_required(login_url=settings.LOGIN_ULR)
def sign_up(request):
    if request.method == "POST":
        username =request.POST.get('name')
        password =request.POST.get('password')
        email =request.POST.get('email')
        check_user = User.objects.filter(username=username)
        if check_user is not None:
            print('checkkkk')
            messages.error(request, ('Tên đăng nhập đã tồn tại!'))
            return render(request,'admin_custom/sign-up.html')
        try:
            user = User.objects.create(username=username,password=password,email = email,is_superuser = True, is_staff = True)
            messages.success(request, ('Đăng ký thành công!'))
        except:
            messages.error(request, ('Đăng ký thất bại!'))
        return render(request,'admin_custom/sign-up.html')
    return render(request,'admin_custom/sign-up.html')


    