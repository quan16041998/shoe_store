from django.db import models

# Create your models here.
from django.db import models
from django.utils.translation import gettext_lazy as _
from django.utils.html import escape
from django.utils.html import mark_safe


TEL_BANNER = '1'
FORM_BANNER = '2'
IMAGE_BANNER = '3'
WEB_BANNER_TYPE = (
    (TEL_BANNER, "Phone-Input Banner"),
    (FORM_BANNER, "Form Banner"),
    (IMAGE_BANNER, "Image-Only Banner"),
)
SALE_PERCENT = (
    ('0',"Không"),
    ('10',"10%"),
    ('20',"20%"),
    ('30',"30%"),
    ('40',"40%"),
    ('50',"50%"),
    ('60',"60%"),
    ('70',"70%"),
    ('80',"80%"),
    ('90',"90%"),
)
STATUS = (
    ('normal',"NORMAL"),
    ('sale',"SALE"),
    ("new",'NEW')
)
STATUS_ORDER =(
    ('new',"Đơn chưa xử lý"),
    ('accepted','Đã nhận đơn'),
    ('ship',"Đang trên đường vận chuyển"),
    ("complated",'Đã hoàn thành')
)

# Create your models here.
class Category(models.Model):
    title = models.CharField(max_length=255,default='')
    slug = models.CharField(max_length=255,default='')
    activate = models.BooleanField(default=True)
    def __str__(self):
        return self.title

class Product(models.Model):
    name = models.CharField(max_length=255,default='')
    category = models.ForeignKey(Category,on_delete=models.CASCADE)
    price = models.IntegerField(default=0)
    sale_percent =models.CharField(choices=SALE_PERCENT,max_length=10,default=1)
    accurate_price = models.IntegerField(default=0)
    fabric = models.CharField(max_length=50,default='')
    describe = models.CharField(max_length=255,default='Miêu tả',null=True)
    status = models.CharField(choices=STATUS,max_length=50,default='normal')
    activate = models.BooleanField(default=True)
    created_at = models.DateTimeField(auto_now=True)
   
    def __str__(self):
        if self.name:
            return self.name
class size_product(models.Model):
    size = models.CharField(max_length=10,default='')
    def __str__(self):
        return self.size   
    
class product_variants(models.Model):
    product_id = models.ForeignKey(Product,on_delete=models.CASCADE)
    size = models.ForeignKey(size_product,on_delete=models.CASCADE)
    quantity  = models.IntegerField(null=False, blank=False)
    sku = models.CharField(max_length=10,default='')
    
  
class Img_product(models.Model):
    id_product = models.ForeignKey(Product,on_delete=models.CASCADE)
    img = models.ImageField( upload_to="products/%Y/%m%d",null=True, blank=True)
    def __str__(self):
        return self.id_product.name
    

    
class Order(models.Model):
    customer_name = models.CharField(max_length=255,default='',null=True,blank=True)
    shipping_address = models.CharField(max_length=255,default='',null=True,blank=True)
    phone_number = models.CharField(max_length=11,default='',null=True,blank=True)
    email = models.EmailField(max_length=254,null=True,blank=True)
    order_description = models.TextField(max_length=255,default='')
    total_price = models.CharField(max_length=255,default='',null=True,blank=True)
    status = models.CharField(choices=STATUS_ORDER ,max_length=10,default='new')
    order_at = models.DateTimeField(auto_now=True)
    def __str__(self):
        return str(self.id)
    
class Order_detail(models.Model):
    order = models.ForeignKey(Order,on_delete=models.CASCADE)
    product = models.ForeignKey(Product,on_delete=models.CASCADE)
    size = models.CharField(max_length=10,null=False, blank=True)
    quantity  = models.IntegerField(null=False, blank=True,default=1)
    def image_tag(self):
            img = Img_product.objects.filter(id_product = self.product).first()
            return mark_safe('<img src="%s" width="125" height="150" />' % (img.img.url))
    image_tag.short_description = 'Image'
    
    
class Banner(models.Model):
    title = models.CharField(
        max_length=255, null=True, blank=True)
    order = models.IntegerField(null=True)
    image = models.ImageField(
        upload_to="banners/%Y/%m%d",
        null=True, blank=True,
        verbose_name=_("Ảnh")
    )
    is_activate  = models.BooleanField(
        default=True, verbose_name="Active", help_text="Active banner", null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True, null=True)
    def __str__(self):
        if self.title:
            return self.title
        else:
            return "Banner "+str(self.id)
        
class About_contact(models.Model):
    title = models.CharField(max_length=255,default='',null=True,blank=True)
    about_describe = models.CharField(max_length=255,default='',null=True,blank=True)
    last_mes = models.CharField(max_length=255,default='',null=True,blank=True)
    facebook =  models.CharField(max_length=255,default='',null=True,blank=True)
    instagram = models.CharField(max_length=255,default='',null=True,blank=True)
    