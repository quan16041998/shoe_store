from django import template
register = template.Library()
from admin_custom.models import Img_product, Banner
from admin_custom.forms import FormBanner


@register.simple_tag(name='get_img_product')
def get_img_product(id_product):
    img = Img_product.objects.filter(id_product = id_product).first()
    if img:
        return img.img.url
    return None

@register.inclusion_tag('admin_custom/edit_banner.html')
def modal_banner(id):
    banner = Banner.objects.get(id = id)
    form = FormBanner(instance=banner)
    context = {
        'form_banner' : form,
        'id'  : id
    }
    return context
    



