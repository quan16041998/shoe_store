from django.contrib import admin

# Register your models here.

from django.contrib import admin
from django import forms
# Register your models here.
from .models import *

class CategoryAdmin(admin.ModelAdmin):
    model = Category
    list_display = ['title']
    search_fields = ['title']
admin.site.register(Category,CategoryAdmin)

class ImgAdmin(admin.TabularInline):
    model = Img_product
    
class ProductVariantsAdmin(admin.TabularInline):
    model = product_variants
 
class ProductAdmin(admin.ModelAdmin):
    model = Product
    list_display = ['name']
    search_fields = ['name']
    inlines = [ImgAdmin, ProductVariantsAdmin]

    class Media:
        js = (
            'index/admin/product-custom.js',  # project static folder
             '//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js',
        )
admin.site.register(Product,ProductAdmin)

class ProductOrderAdmin(admin.TabularInline):
    model = Order_detail
    fields = ( 'image_tag', )
    readonly_fields = ('image_tag',)
 


class OrderAdmin(admin.ModelAdmin):
    model = Order
    inlines = [ProductOrderAdmin]
admin.site.register(Order,OrderAdmin)

class SizeAdmin(admin.ModelAdmin):
    model = size_product
admin.site.register(size_product,SizeAdmin)


class AboutContactAdmin(admin.ModelAdmin):
    model = About_contact
admin.site.register(About_contact,AboutContactAdmin)

class BannerAdmin(admin.ModelAdmin):
    model = Banner
    list_filter = ['is_activate']
    search_fields = ['title']
    list_per_page = 10
    readonly_fields = ['created_at']
    ordering = ['id',]
    db_table = "seo_banner"
    verbose_name = "Quản lý banner"
admin.site.register(Banner, BannerAdmin)


      

