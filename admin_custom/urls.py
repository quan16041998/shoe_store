from django.urls import path
from . import views
from django.conf import settings
from django.contrib.auth import views as auth_views
app_name='admin_custom'
urlpatterns = [
path('admin_product/',views.product_table,name='admin_product'),
path('admin_order/',views.order_table,name='admin_order'),
path('admin_other/',views.admin_other,name='admin_other'),
path('add_product/',views.add_product,name='add_product'),
path('add_size/',views.add_size,name='add_size'),
path('add_category/',views.add_category,name='add_category'),
path('add_banner/',views.add_banner,name='add_banner'),
path('edit_banner/',views.edit_banner,name='edit_banner'),
path('delete_product/',views.delete_product,name='delete_product'),
path('delete_order/',views.delete_order,name='delete_order'),
path('delete_other/',views.delete_other,name='delete_other'),
path('edit_product/<int:product_id>/',views.edit_product,name='edit_product'),
path('edit_order/<int:order_id>/',views.edit_order,name='edit_order'),
path('edit_about',views.edit_about_contact,name='edit_about'),
path('login/',views.sign_in,name='login'),
path('logout/',views.sign_out,name='logout'),
path('signup/', views.sign_up,name="sign_up"),
]