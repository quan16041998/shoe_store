# Generated by Django 3.1.7 on 2023-04-21 09:27

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='About_contact',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(blank=True, default='', max_length=255, null=True)),
                ('about_describe', models.CharField(blank=True, default='', max_length=255, null=True)),
                ('last_mes', models.CharField(blank=True, default='', max_length=255, null=True)),
                ('facebook', models.CharField(blank=True, default='', max_length=255, null=True)),
                ('instagram', models.CharField(blank=True, default='', max_length=255, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='Banner',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(blank=True, max_length=255, null=True)),
                ('order', models.IntegerField(null=True)),
                ('image', models.ImageField(blank=True, null=True, upload_to='banners/%Y/%m%d', verbose_name='Banner Image Desktop')),
                ('banner_type', models.CharField(choices=[('1', 'Phone-Input Banner'), ('2', 'Form Banner'), ('3', 'Image-Only Banner')], default='1', max_length=255, verbose_name='Type')),
                ('is_activate', models.BooleanField(blank=True, default=True, help_text='Active banner', null=True, verbose_name='Active')),
                ('created_at', models.DateTimeField(auto_now_add=True, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='Category',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(default='', max_length=255)),
                ('slug', models.CharField(default='', max_length=255)),
                ('activate', models.BooleanField(default=True)),
            ],
        ),
        migrations.CreateModel(
            name='Order',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('customer_name', models.CharField(blank=True, default='', max_length=255, null=True)),
                ('shipping_address', models.CharField(blank=True, default='', max_length=255, null=True)),
                ('phone_number', models.CharField(blank=True, default='', max_length=11, null=True)),
                ('email', models.EmailField(blank=True, max_length=254, null=True)),
                ('order_description', models.TextField(default='', max_length=255)),
                ('total_price', models.CharField(blank=True, default='', max_length=255, null=True)),
                ('is_complated', models.BooleanField(default=False)),
            ],
        ),
        migrations.CreateModel(
            name='Product',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(default='', max_length=255)),
                ('price', models.IntegerField(default=0)),
                ('sale_percent', models.CharField(choices=[('0', 'Không'), ('10', '10%'), ('20', '20%'), ('30', '30%'), ('40', '40%'), ('50', '50%'), ('60', '60%'), ('70', '70%'), ('80', '80%'), ('90', '90%')], default=1, max_length=10)),
                ('accurate_price', models.IntegerField(default=0)),
                ('size', models.CharField(default='', max_length=50)),
                ('fabric', models.CharField(default='', max_length=50)),
                ('describe', models.CharField(default='Miêu tả', max_length=255, null=True)),
                ('status', models.CharField(choices=[('normal', 'NORMAL'), ('sale', 'SALE'), ('new', 'NEW')], default='normal', max_length=50)),
                ('activate', models.BooleanField(default=True)),
                ('created_at', models.DateTimeField(auto_now=True)),
                ('category', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='admin_custom.category')),
            ],
        ),
        migrations.CreateModel(
            name='size_product',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('size', models.CharField(default='', max_length=10)),
            ],
        ),
        migrations.CreateModel(
            name='product_variants',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('quantity', models.IntegerField()),
                ('sku', models.CharField(default='', max_length=10)),
                ('product_id', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='admin_custom.product')),
                ('size', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='admin_custom.size_product')),
            ],
        ),
        migrations.CreateModel(
            name='Order_detail',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('order', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='admin_custom.order')),
                ('product', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='admin_custom.product')),
            ],
        ),
        migrations.CreateModel(
            name='Img_product',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('img', models.ImageField(blank=True, null=True, upload_to='products/%Y/%m%d')),
                ('id_product', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='admin_custom.product')),
            ],
        ),
    ]
